User: [User: myjinxin2015](https://www.codewars.com/users/myjinxin2015)
```js
const switcheroo=x=>x.replace(/[ab]/g,x=>x=="a"?"b":"a")
```

User: [tvolf](https://www.codewars.com/users/tvolf)
```js
function switcheroo(x){
  return x.replace(/[ab]/g, function(c) { return c === 'a' ? 'b' : 'a'; });
}
```

User: [wisyr](https://www.codewars.com/users/wisyr)
```js
function switcheroo(x){
  return [...x].map(v => v == 'a' ? v = 'b' : v == 'b' ? v = 'a' : v).join('');
}
```