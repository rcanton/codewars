function switcheroo(x){
  return x.split('').map(i => i === 'a' ? 'b' : i === 'b' ? 'a' : 'c').join('');
}